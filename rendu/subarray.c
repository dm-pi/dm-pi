/*****************************************************************/
/*                                                               */
/*     DM d'ALGO PARALLELE : SUBARRAY PROBLEM                    */
/*                                                               */
/*            Lucca Hirschi & Thomas Sibut-Pinote                */
/*                                                               */
/*****************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <mpi.h>
#include <math.h>
#include <stdarg.h>
#include <time.h>

int MIN(int a, int b){if (a<b) return a; else return b;};
int MAX(int a, int b){if (a<b) return b; else return a;};


/*******************************************************************************/
/* Structure de donn�e poss�d�e par chaque processeur de la grille 2D de procs */
/*******************************************************************************/
typedef struct { 
  int row;   /* le nombre de lignes */
  int col;   /* le nombre de colonnes */
  int* data; /* les valeurs de la sous-matrices du processeur */
  int* r;    /* les registres r de la sous-matrices du processeur */
  int* s;    /* les registres s de la sous-matrices du processeur */
  int* mini; /* les registres min de la sous-matrices du processeur */
  int* M;    /* les registres M de la sous-matrices du processeur */
  int* cand; /* les registres cand de la sous-matrices du processeur */
  int* flag_active; /* savoir si les cellules de la sous-matrices du processeur sont actives ou non (oui -> 1, non -> 0) */
  /* les indices du coin en haut � gauche et du coin en bas � droite de la sous-matrice maximale COURANTE */
  int* rang_x_max;
  int* rang_y_max;
  int* rang_x_max2;
  int* rang_y_max2;
  /* les indices du coin en haut � gauche (coin en bas � droite = nous) de la sous-matrice qui correspond au cand courant */
  /* d�fini � chaque affectation de mini ! */
  int* rang_x;
  int* rang_y;
  /* les entr�es pour chaque registre_in d'en haut */
  int* r_buff_h;
  int* s_buff_h;
  int* M_buff_h;
  int* mini_buff_h;
  int* flag_buff_h;
  int* rang_x_m_buff_h;		/* buffer de rang_x_max */
  int* rang_y_m2_buff_h;	/* buffer de rang_y_max */
  int* rang_x_m2_buff_h;	/* buffer de rang_x_max2 */
  int* rang_y_m_buff_h;		/* buffer de rang_y_max2 */
  int* rang_x_buff_h;		/* buffer de rang_x */
  int* rang_y_buff_h;		/* buffer de rang_y */
  /* les entr�es pour chaque registre_in de la gauche */
  int* r_buff_g;
  int* s_buff_g;
  int* M_buff_g;
  int* mini_buff_g;
  int* flag_buff_g;
  int* rang_x_m_buff_g;		/* buffer de rang_x_max */
  int* rang_y_m_buff_g;		/* buffer de rang_y_max */
  int* rang_x_m2_buff_g;        /* buffer de rang_x_max2 */
  int* rang_y_m2_buff_g;	/* buffer de rang_y_max2 */
  int* rang_x_buff_g;		/* buffer de rang_x */
  int* rang_y_buff_g;		/* buffer de rang_y */
  /* pour les flag: les flag � transmettre en bas et � droite au prochain tour */
  int* flag_buff_b;
  int* flag_buff_d;
} matrix_t;

// --variables globales -- //
int* matrice;	         	/* matrice du probl�me  */
int ncols, nrows;		/* taille de cette matrice */
int my_id, nb_proc;		/* le  num�ro du processeur et le nombre total de proc�sseurs */
MPI_Comm MPI_HORIZONTAL; /* Communicateur pour diffusions horizontales */
MPI_Comm MPI_VERTICAL;   /* Communicateur pour diffusions verticales */
MPI_Status statut_null;	 /* un statut pour les Receive, ne nous servira pas */
int i_col,i_row;         /* Position dans la grille 2D de procs */
int taille_grille;       /* nombre de procs sur une ligne ou une colonne dans la grille 2Ds de processeurs*/


/*******************************************/
/* affichage de la matrice                 */
/*******************************************/
void mat_display(matrix_t* A, int flag)
{
  int i,j,t=0;
  printf("      ");
  for(j=0;j<A->col;j++)
    printf(" %.1d ",j);
  printf("\n");
  printf("    __");
  for(j=0;j<A->col;j++)
    printf("____");
  printf("_\n");

  for(i=0;i<A->row;i++)
    {
      printf("%.1d | ",i);
      for(j=0;j<A->col;j++)
	{
	  if (flag==1)
	    {  
	      if (A->M[t]>=0)
		printf(" %.1d ",A->M[t]);
	      else
		printf("%.1d ",A->M[t]);
	    }
	  if(flag == 0)
	    {
	      if (A->data[t]>=0)
		printf(" %.1d ",A->data[t]);
	      else
		printf("%.1d ",A->data[t]);
	    }
	  if(flag == 2)
	    {
	      if (A->mini[t]>=0)
		printf(" %.1d ",A->mini[t]);
	      else
		printf("%.1d ",A->mini[t]);
	    }
	  if(flag == 3)
	    {
	      if (A->s[t]>=0)
		printf(" %.1d ",A->s[t]);
	      else
		printf("%.1d ",A->s[t]);
	    }
	  if(flag ==4)
	    {
	      if (A->r[t]>=0)
		printf(" %.1d ",A->r[t]);
	      else
		printf("%.1d ",A->r[t]);
	    }
	  if(flag == 5)
	    {
	      if (A->cand[t]>=0)
		printf(" %.1d ",A->cand[t]);
	      else
		printf("%.1d ",A->cand[t]);
	    }
	  if(flag == 6)
	    {
	      if (A->flag_active[t]>=0)
		printf(" %.1d ",A->flag_active[t]);
	      else
		printf("%.1d ",A->flag_active[t]);
	    }

	  t++;
	}
      printf("|\n");
    }
  printf("    --");
  for(j=0;j<A->col;j++)
    printf("----");
  printf("-\n");
};


void affiche_spam (matrix_t* A, int flag) {
  if (flag == 1)
    printf("AFFICHE M de %d\n ------------------------------------------------------------\n",my_id);
  if (flag == 0)	
    printf("AFFICHE DATA de %d\n ------------------------------------------------------------\n",my_id);
  if (flag == 2)	
    printf("AFFICHE Mini de %d\n ------------------------------------------------------------\n",my_id);
  if (flag == 3)	
    printf("AFFICHE s de %d\n ------------------------------------------------------------\n",my_id);
  if (flag == 4)	
    printf("AFFICHE r de %d\n ------------------------------------------------------------\n",my_id);
  if (flag == 5)	
    printf("AFFICHE cand de %d\n ------------------------------------------------------------\n",my_id);
  if (flag == 6)	
    printf("AFFICHE flag_active de %d\n ------------------------------------------------------------\n",my_id);
  mat_display(A, flag);
}


/*******************************************/
/* SOUS FONCTIONS DE UPDATE EN ITERATIF    */
/*******************************************/
// -- Fonctions de positions -- //
int position_h(int col, int i, int j)
{
  return col * (i-1) + j;
}

int position_b(int col, int i, int j)
{
  return col * (i+1) + j;
}

int position(int col, int i, int j)
{
  return col * i + j;
}

// -- fonction servants � la mise � jour du registre M en g�rant les coordonn�es du meilleur rectangle -- //
int maximum(matrix_t* A, int col, int haut,int gauche, int ici, int cand_ici, int flag[4], int i, int j, int ii, int jj)
{
  /* calcule le max  parmis les flag[i] = 1 et affecte les coordonnees rang_x_max (y) en fct de celui qui est le max */
  int maxi = haut;
  int ind_maxi = 0;
  int k;
  int t[4] = {haut,gauche,ici,cand_ici};
  int pos = position(col, i, j);
  for(k=0;k<4;k++)
    {
      if ((t[k] > maxi && flag[k] == 1) || (flag[0] == 0 && (ind_maxi == 0))) /* deuxi�me ou permet de ne pas tenir compte de l'affectation initiale maxi = haut */
	{
	  maxi = t[k];
	  ind_maxi = k;
	}
    }
  // faire des affectations du rectangle en fonction du max choisi
  if (ind_maxi == 0)	/* si le max nous vient d'en haut */
    {
      if (i == 0)		/* si je suis tout en haut */
	{
	  A->rang_x_max[pos] = A->rang_x_m_buff_h[j];
	  A->rang_y_max[pos] = A->rang_y_m_buff_h[j];
	  A->rang_x_max2[pos] = A->rang_x_m2_buff_h[j];
	  A->rang_y_max2[pos] = A->rang_y_m2_buff_h[j];
	}
      else
	{
	  A->rang_x_max[pos] = A->rang_x_max[position_h(col, i, j)];
	  A->rang_y_max[pos] = A->rang_y_max[position_h(col, i, j)];
	  A->rang_x_max2[pos] = A->rang_x_max2[position_h(col, i, j)];
	  A->rang_y_max2[pos] = A->rang_y_max2[position_h(col, i, j)];
	}
    }
  else if (ind_maxi == 1)	/* si le max nous vient de la gauche */
    {
      if (j == 0)		/* si je suis tout � gauche */
	{
	  A->rang_x_max[pos] = A->rang_x_m_buff_g[i];
	  A->rang_y_max[pos] = A->rang_y_m_buff_g[i];
	  A->rang_x_max2[pos] = A->rang_x_m2_buff_g[i];
	  A->rang_y_max2[pos] = A->rang_y_m2_buff_g[i];
	}
      else
	{
	  A->rang_x_max[pos] = A->rang_x_max[pos - 1];
	  A->rang_y_max[pos] = A->rang_y_max[pos - 1];
	  A->rang_x_max2[pos] = A->rang_x_max2[pos - 1];
	  A->rang_y_max2[pos] = A->rang_y_max2[pos -1];
	}
    }
  else if (ind_maxi == 3)	/* si le max est un nouveau MAX (cand) */
    {
      /* le coin en bas � droite = NOUS */
      A->rang_x_max2[pos] = ii + i;
      A->rang_y_max2[pos] = jj + j;
      /* le coin en haut � gauche = le coin d�fini par rang_x et rang_y (assign�s durant les affectation de mini) */
      A->rang_x_max[pos] = A->rang_x[pos];
      A->rang_y_max[pos] = A->rang_y[pos];
    }
  /* dernier cas: on ne fait rien car le max reste le notre */
 
 return maxi;
}

// -- met � jour le registre r --//
void update_r(int col, int* r, int* data, int* r_buff_g, int left_empty,int i, int j) /* v�rifi� */
{
  int pos = position(col,i,j);
  if (j==0)
    {
      if (left_empty)
	{
	  r[pos] = data[pos];
	}
      else
	{
	  r[pos] = data[pos] + r_buff_g[i];
	}
    }
  else //j>1
    {
      r[pos] = data[pos] + r[pos-1];
    }
};


// -- met � jour le registre mini -- //
void update_mini(matrix_t* A, int col, int* data, int* mini, int* s_buff_g, int* mini_buff_g, int* s, int left_empty, int i, int j, int ii, int jj) /* v�rifi� */
{
  int pos = position(col,i,j);
  if (j==0)
    {
      if (left_empty)
	{
	  mini[pos] = 0;
	  /* mettre � jour les coordonn�es hautes de rang_x (y) */
	  A->rang_x[pos] = ii + i;
	  A->rang_y[pos] = jj + j;
	}
      else
	{
	  int min;
	  if (s_buff_g[i] < mini_buff_g[i])
	    {
	      mini[pos] = s_buff_g[i];
	      /* mettre � jour les coordonn�es hautes de rang_x (y) */	     
 	      A->rang_x[pos] = ii + i;
	      A->rang_y[pos] = jj + j - 1;
	    }
	  else
	    {
	      mini[pos] = mini_buff_g[i];
	      /* mettre � jour les coordonn�es hautes de rang_x (y) */
	      A->rang_x[pos] = A->rang_x_buff_g[i];
	      A->rang_y[pos] = A->rang_y_buff_g[i];
	    }
	}
    }
  else // j > 1
    {
      mini[pos] = MIN (s[pos-1], mini[pos-1]);
      int min;
      if (s[pos-1] < mini[pos-1])
	{
	  mini[pos] = s[pos-1];
	  /* mettre � jour les coordonn�es hautes de rang_x (y) */
	  A->rang_x[pos] = ii + i;
	  A->rang_y[pos] = jj + j - 1;
	}
      else
	{
	  mini[pos] = mini[pos-1];
	  /* mettre � jour les coordonn�es hautes de rang_x (y) */
	  A->rang_x[pos] = A->rang_x[pos - 1];
	  A->rang_y[pos] = A->rang_y[pos - 1];
	}
    }
}

// -- met � jour le registre s -- //
void update_s(int col, int* s_buff_h, int* r, int* s, int up_empty, int i, int j) /* v�rifi� */
{
  int pos = position(col,i,j);
  if(i==0)
    {
      if (up_empty)
	{
	  s[pos] = r[pos];
	}
      else
	{
	  s[pos] = s_buff_h[j] + r[pos];
	}
    }
  else // i > 0
    {
      s[pos] = s[position_h(col,i,j)] + r[pos];
    }
}

// -- met � jour le registre M -- //
void update_M(matrix_t* A, int col, int* M, int* M_buff_h, int* M_buff_g, int* cand, int up_empty, int left_empty, int i, int j, int ii, int jj) /* v�rifi� */
{
  int pos = position(col,i,j);
  int flag[4] = {1,1,1,1};
  if(i==0)
    {
      if (j==0)
	{
	  if (up_empty)
	    {
	      flag[0] = 0;
	    }
	  if (left_empty)
	    {
	      flag[1] = 0;
	    }
	  M[pos] = maximum(A, col, M_buff_h[j],M_buff_g[i],M[pos],cand[pos],flag, i, j, ii, jj);
	}
      else // j> 0 et i = 0
	{
	   if (up_empty)
	    {
	      flag[0] = 0;
	    }
	   M[pos] = maximum(A, col,M_buff_h[j],M[pos-1],M[pos],cand[pos],flag, i, j, ii, jj);
	}
    }
  else // i>0
    {
      if (j==0)
	{
	  if (left_empty)
	    {
	      flag[1] = 0;
	    }
	  M[pos] = maximum(A, col,M[pos - col],M_buff_g[i],M[pos],cand[pos],flag, i, j, ii, jj);
	}
      else // i > 0 et j > 0
	{
	  M[pos] = maximum(A, col,M[pos-col],M[pos-1],M[pos],cand[pos],flag, i, j, ii, jj);
	}
    }
}

// -- active les registres en local -- //
void activate(int col, int row, int* flag_buff_b, int* flag_buff_d , int* flag_active, int i, int j)
{
  // il faut activer si possible (i,j+1)
  // PROPAGATION DU CONTROLE SEULEMENT VERS LA DROITE !
  int pos = position(col,i,j);
  if (j+1<col)
    {
      flag_active[pos+1] = 1;
    }
  else // on est au bord � droite
    {
      flag_buff_d[i] = 1;
    }
}

// -- met � jour enti�rement une case en local -- //
void update_case(matrix_t* A,int left_empty,int up_empty,int right_empty,int down_empty, int i, int j, int ii, int jj)
{		
  int pos = position(A->col,i,j);
  if (A->flag_active[pos] == 1)
    {
      // activer les processeurs
      activate(A->col, A->row, A->flag_buff_b, A->flag_buff_d, A->flag_active, i, j);
      // mettre � jour les registres de la case
      update_r(A->col, A->r, A->data, A->r_buff_g, left_empty, i, j);
      update_mini(A, A->col, A->data, A->mini, A->s_buff_g, A->mini_buff_g, A->s, left_empty, i, j, ii, jj);
      update_s(A->col, A->s_buff_h, A->r, A->s, up_empty, i, j);
      A->cand[pos] = A->s[pos] - A->mini[pos];
      update_M(A, A->col, A->M, A->M_buff_h, A->M_buff_g, A->cand, up_empty, left_empty, i, j, ii, jj);
    }
}


// -- pr�pare l'activation de cases sur d'autres processeurs -- //
void activation_bord (matrix_t* A, int i, int j)
{
  int pos = position(A->col, i, j);
  // PROPAGATION DU CONTROLE SEULEMENT VERS LA DROITE !
  if (j == 0 && A->flag_buff_g[i] == 1)
    {
      A->flag_active[pos] = 1;
    }
}


/*******************************************/
/* update en it�ratif                      */
/*******************************************/

void update_ite(matrix_t* A, int left_empty, int up_empty, int right_empty, int down_empty)
{
  /* cordoonnes du processeurs dans la grille 2D (r�f�rentiel = la matrice du probl�me) */
  int ii;
  int jj;
  MPI_Comm_rank(MPI_HORIZONTAL, &jj);
  MPI_Comm_rank(MPI_VERTICAL, &ii);

  /* TODO: ici il faudrait faire gaffe: cas du processeur en fin de ligne ou colonne qui n'a pas les m�mes dimensions  AVEC NROW/TAILLE_GRILLE C'eST OK NON?*/
  ii = ii * nrows/taille_grille;		/* DU COUP OK. oui car avant ce proc, toutes les sous-matrices ont la m�me dimension-> NON! */
  jj = jj * ncols/taille_grille;		        /* DU COUP? OK .oui car avant ce proc, toutes les sous-matrices ont la m�me dimension-> NON! */
  /* coordonnes de la case DANS ce processeur */
  int j,i;		
  for(i = A->row-1; i>=0; i--)
    {
      for(j = A->col - 1; j >= 0; j--)
	{
	  activation_bord(A, i, j);
	  update_case(A,left_empty,up_empty,right_empty,down_empty,i,j, ii, jj);
	}
    }
}


/*******************************************/
/* Initialisation de la grille             */
/*******************************************/
void mat_init(matrix_t *mat,int width, int height)
{
  int i,rh,rv;
  int j;
  mat->row = height;
  mat->col = width;

  MPI_Comm_rank(MPI_VERTICAL,&rv); //ligne
  MPI_Comm_rank(MPI_HORIZONTAL,&rh); // colonne
  
  /* Alloc  */
  mat->data=(int*)calloc(height*width,sizeof(int));
  mat->r=(int*)calloc(height*width,sizeof(int));
  mat->s=(int*)calloc(height*width,sizeof(int));
  mat->mini=(int*)calloc(height*width,sizeof(int));
  mat->M=(int*)calloc(height*width,sizeof(int));
  mat->cand=(int*)calloc(height*width,sizeof(int));
  mat->flag_active=(int*)calloc(height*width,sizeof(int));

  mat->rang_x_max=(int*)calloc(height*width,sizeof(int));
  mat->rang_y_max=(int*)calloc(height*width,sizeof(int));
  mat->rang_x_max2=(int*)calloc(height*width,sizeof(int));
  mat->rang_y_max2=(int*)calloc(height*width,sizeof(int));
  mat->rang_x=(int*)calloc(height*width,sizeof(int));
  mat->rang_y=(int*)calloc(height*width,sizeof(int));

  mat->r_buff_h=(int*)calloc(width,sizeof(int));
  mat->s_buff_h=(int*)calloc(width,sizeof(int));
  mat->M_buff_h=(int*)calloc(width,sizeof(int));
  mat->mini_buff_h=(int*)calloc(width,sizeof(int));
  mat->flag_buff_h=(int*)calloc(width,sizeof(int));
  mat->rang_x_m_buff_h=(int*)calloc(width,sizeof(int));
  mat->rang_y_m_buff_h=(int*)calloc(width,sizeof(int));
  mat->rang_x_m2_buff_h=(int*)calloc(width,sizeof(int));
  mat->rang_y_m2_buff_h=(int*)calloc(width,sizeof(int));
  mat->rang_x_buff_h=(int*)calloc(width,sizeof(int));
  mat->rang_y_buff_h=(int*)calloc(width,sizeof(int));

  mat->r_buff_g=(int*)calloc(height,sizeof(int));
  mat->s_buff_g=(int*)calloc(height,sizeof(int));
  mat->M_buff_g=(int*)calloc(height,sizeof(int));
  mat->mini_buff_g=(int*)calloc(height,sizeof(int));
  mat->flag_buff_g=(int*)calloc(height,sizeof(int));
  mat->rang_x_m_buff_g=(int*)calloc(height,sizeof(int));
  mat->rang_y_m_buff_g=(int*)calloc(height,sizeof(int));
  mat->rang_x_m2_buff_g=(int*)calloc(height,sizeof(int));
  mat->rang_y_m2_buff_g=(int*)calloc(height,sizeof(int));
  mat->rang_x_buff_g=(int*)calloc(height,sizeof(int));
  mat->rang_y_buff_g=(int*)calloc(height,sizeof(int));

  mat->flag_buff_b=(int*)calloc(width,sizeof(int));
  mat->flag_buff_d=(int*)calloc(height,sizeof(int));

  /* Affectation init */
  for(i=0 ; i<height ; i++)
    {
      for (j=0; j<width; j++)
	{
	  int pos = i*width + j;
	  mat->data[pos] = 0 ;
	  mat->r[pos] = 0;
	  mat->s[pos] = 0;
	  mat->mini[pos] = 0;
	  mat->M[pos] = 0;
	  mat->cand[pos] = 0;
	  mat->flag_active[pos] = 0;
	  
	  /* on fait attention: le processeur en fin de ligne ou en fin de colonne n'a pas les m�ms dimensions que les autres */
	  mat->rang_x_max[pos] = rh * (ncols/taille_grille) + j;
	  mat->rang_y_max[pos] = rv * (nrows/taille_grille) + i;
	  mat->rang_x_max2[pos] = rh * (ncols/taille_grille) + j;
	  mat->rang_y_max2[pos] = rv * (nrows/taille_grille) + i;
	  mat->rang_x[pos] = rh * (ncols/taille_grille) + j;
	  mat->rang_y[pos] = rv * (nrows/taille_grille) + i;
	}
    }
  for (i = 0; i < width; i++)
    {
      mat->r_buff_h[i] = 0;
      mat->s_buff_h[i] = 0;
      mat->M_buff_h[i] = 0;
      mat->mini_buff_h[i] = 0;
      mat->flag_buff_h[i] = 0;
      mat->rang_x_m_buff_h[i] = 0;
      mat->rang_y_m_buff_h[i] = 0;
      mat->rang_x_m2_buff_h[i] = 0;
      mat->rang_y_m2_buff_h[i] = 0;
      mat->rang_x_buff_h[i] = 0;
      mat->rang_y_buff_h[i] = 0;

      mat->flag_buff_b[i] = 0;
    }
  for (i = 0; i < height; i++)
    {   
      mat->r_buff_g[i] = 0;
      mat->s_buff_g[i] = 0;
      mat->M_buff_g[i] = 0;
      mat->mini_buff_g[i] = 0;
      mat->flag_buff_g[i] = 0;
      mat->rang_x_m_buff_g[i] = 0;
      mat->rang_y_m_buff_g[i] = 0;
      mat->rang_x_m2_buff_g[i] = 0;
      mat->rang_y_m2_buff_g[i] = 0;
      mat->rang_x_buff_g[i] = 0;
      mat->rang_y_buff_g[i] = 0;

      mat->flag_buff_d[i] = 0;
    }      
  
  if (rh == 0)
    {
      for (i = 0; i < height; i++)
	{
	  mat->flag_active[i*width] = 1; /* la premi�re colonne de processeurs est initialement activ�e */
	}
    }
}


/**********************************/
/* Communications  (pour update)  */
/**********************************/
// --  envoie de la derni�re colonne de donnes dans a_recevoir via buff_c (on transf�re dabord cette colonne dans buff_c) -- //
void com_col (int ligne, int colonne, int* buff_c, int col, int row, int* a_recevoir, int* donnees, int flag)
{  
  /* flag = 1 : donnee de taille col*row, flag = 0 : donnee de taille row */
  int i;
  if (colonne < taille_grille - 1)  	/* si je ne suis pas tout � droite alors: */
    {
      if (flag)			/* alors il faut bufferis� acant d'envoyer */
	{	
	  for (i=0; i < row; i++) /* on remplit buff_l avec la derni�re colonne de donnes */
	    {
	      buff_c[i] = donnees[i*col + col-1]; /* le dernier �l�ment de chaque ligne */
	    }
	  MPI_Send(buff_c, row, MPI_INT, colonne + 1, 1, MPI_HORIZONTAL);
	}
      else
	{
	  MPI_Send(donnees, row, MPI_INT, colonne + 1, 1, MPI_HORIZONTAL);
	}
    }
  if (colonne > 0)	/* si je ne suis pas tout � gauche */
    {
      MPI_Recv(a_recevoir, row, MPI_INT, colonne - 1, 1, MPI_HORIZONTAL, &statut_null);
    }
  
  if (taille_grille > 1)	/* si il y a au moins 2 processeurs */
    MPI_Barrier(MPI_HORIZONTAL);
}

// --  envoie de la derni�re ligne de donnes dans a_recevoir -- //
void com_row (int ligne, int colonne, int col, int row, int* a_recevoir, int* donnees, int flag)
{
  /* flag = 1 : donnee de taille col*row, flag = 0 : donnee de taille col */
  if (ligne < taille_grille - 1)  	/* si je ne suis pas tout en bas alors: */
    {
      if (flag)
	{
	  /* avec + (row -1)*col,col on �crit qu'on envoie la derni�re ligne seulement */
	  MPI_Send(donnees + (row - 1)*col, col, MPI_INT, ligne + 1, 1, MPI_VERTICAL);
	}
      else
	{
	  MPI_Send(donnees, col, MPI_INT, ligne + 1, 1, MPI_VERTICAL);
	}
    }
  if (ligne > 0)	/* si je ne suis pas tout en haut */
    {
      MPI_Recv(a_recevoir, col, MPI_INT, ligne -1, 1, MPI_VERTICAL, &statut_null);
    }
  if (taille_grille > 1)	/* si il y a au moins deux processeurs */
    MPI_Barrier(MPI_VERTICAL);
}


/***************************************/
/*  update                             */
/***************************************/
void update(matrix_t* A)
{
  int k;
  int colonne;
  int ligne;
  /* on r�cup�re les coordonn�es du processeur dans la grille 2-D */
  MPI_Comm_rank(MPI_HORIZONTAL, &colonne);
  MPI_Comm_rank(MPI_VERTICAL, &ligne);
  
  int buff_c[A->row];	/* juste pour ici: contiendra la derni�re colonne d'un des registres r, s ou M */  
  for (k = 0; k < (ncols + nrows); k++) /* todo v�rifier le nombre de tours qu'il faut */
    {
      // -- Premi�re �tape: on calcule localement et sans communication ext�rieure update pour nos cellules internes -- //
      // -------------------------------------------------------------------------------------------------------------- //
      update_ite(A, (colonne == 0), (ligne == 0), (colonne == taille_grille - 1), (ligne == taille_grille - 1));


      // -- Deuxi�me �tape: on communique avec nos voisins: d'abord � gauche (E, O) puis en haut (S, N) -- //
      // ------------------------------------------------------------------------------------------------- //

      /* -- send et receive � droite -- */      
      /* envoie de r */
      com_col(ligne, colonne, buff_c, A->col, A->row, A->r_buff_g, A->r, 1);
      /* envoie de s */
      com_col(ligne, colonne, buff_c, A->col, A->row, A->s_buff_g, A->s, 1);
      /* envoie de M */
      com_col(ligne, colonne, buff_c, A->col, A->row, A->M_buff_g, A->M, 1);
      /* envoie de mini */
      com_col(ligne, colonne, buff_c, A->col, A->row, A->mini_buff_g, A->mini, 1);
      /* envoie de flag_active */
      com_col(ligne, colonne, buff_c, A->col, A->row, A->flag_buff_g, A->flag_buff_d, 0); /* ici on a un tableau expr�s */
      /* envoie de rang_x_max */
      com_col(ligne, colonne, buff_c, A->col, A->row, A->rang_x_m_buff_g, A->rang_x_max, 1);
      /* envoie de rang_y_max */
      com_col(ligne, colonne, buff_c, A->col, A->row, A->rang_y_m_buff_g, A->rang_y_max, 1);
      /* envoie de rang_x_max2 */
      com_col(ligne, colonne, buff_c, A->col, A->row, A->rang_x_m2_buff_g, A->rang_x_max2, 1);
      /* envoie de rang_y_max2 */
      com_col(ligne, colonne, buff_c, A->col, A->row, A->rang_y_m2_buff_g, A->rang_y_max2, 1);
      /* envoie de rang_x */
      com_col(ligne, colonne, buff_c, A->col, A->row, A->rang_x_buff_g, A->rang_x, 1);
      /* envoie de rang_y */
      com_col(ligne, colonne, buff_c, A->col, A->row, A->rang_y_buff_g, A->rang_y, 1);

	    
      // -- send et receive en bas -- //
      /* envoie de r */
      com_row(ligne, colonne, A->col, A->row, A->r_buff_h, A->r, 1);
      /* envoie de s */
      com_row(ligne, colonne, A->col, A->row, A->s_buff_h, A->s, 1);
      /* envoie de M */
      com_row(ligne, colonne, A->col, A->row, A->M_buff_h, A->M, 1);
      /* envoie de mini */
      com_row(ligne, colonne, A->col, A->row, A->mini_buff_h, A->mini, 1);
      /* envoie de flag_active */
      com_row(ligne, colonne, A->col, A->row, A->flag_buff_h, A->flag_buff_b, 0); /* ici on a un tableau expr�s */
      /* envoie de rang_x_max */
      com_row(ligne, colonne, A->col, A->row, A->rang_x_m_buff_h, A->rang_x_max, 1);
      /* envoie de rang_y_max */
      com_row(ligne, colonne, A->col, A->row, A->rang_y_m_buff_h, A->rang_y_max, 1);
      /* envoie de rang_x_max */
      com_row(ligne, colonne, A->col, A->row, A->rang_x_m2_buff_h, A->rang_x_max2, 1);
      /* envoie de rang_y_max */
      com_row(ligne, colonne, A->col, A->row, A->rang_y_m2_buff_h, A->rang_y_max2, 1);
      /* envoie de rang_x */
      com_row(ligne, colonne, A->col, A->row, A->rang_x_buff_h, A->rang_x, 1);
      /* envoie de rang_y */
      com_row(ligne, colonne, A->col, A->row, A->rang_y_buff_h, A->rang_y, 1);
    }
} 



/**************************************************/
/* cr�er les communicateurs et r�partir les procs */
/**************************************************/
void init_communicateurs()
{
  /* taille_grille: dimension de la grille car�e de processeurs */
  int ligne,colonne;			      /* les coordonnees � assigner du processeur dans la grille */
  if (my_id > taille_grille*taille_grille-1) /* un processeur d�chet qui ne nous servira pas */
    {
      colonne = 0;
      ligne = nb_proc + my_id; /* cad je suis tt seul dans mon communicateur */
    }
  else
    {  
      ligne = my_id /taille_grille; // l'indice de la ligne
      colonne = my_id - (ligne * taille_grille); // l'indice de la colonne
    }

  /* Cr�ation des communicateurs */
  MPI_Comm_split ( MPI_COMM_WORLD, ligne, colonne, &MPI_HORIZONTAL); /* une ligne de proc (m�me ligne, num�ro = colonne) */

  if (my_id > taille_grille*taille_grille-1) /* un processeur d�chet qui ne nous servira pas */
    {
      colonne = nb_proc + my_id;  /* cad je suis tt seul dans mon communicateur */
      ligne = 0;
    }
  MPI_Comm_split ( MPI_COMM_WORLD, colonne, ligne, &MPI_VERTICAL);   /* une colonne de proc (m�me colonne, num�ro = ligne) */
}


/**************************************************/
/*                                                */
/*              MAIN                              */
/*                                                */
/**************************************************/
int main(int argc, char **argv) 
{
  MPI_Init(&argc,&argv);  
  MPI_Comm_size(MPI_COMM_WORLD,&nb_proc);
  MPI_Comm_rank(MPI_COMM_WORLD,&my_id);
  
  // -- Le processeur 0 r�cup�re la matrice du probl�me en local -- //
  // -------------------------------------------------------------- //
  if(argc==0)
    fprintf(stderr,"Usage : %s <nom_fichier_matrice>",argv[0]);
  else 
    {
      FILE* fichier = fopen(argv[1],"r"); // tout le monde acc�de au fichier pour acc�der aux dimensions
      fscanf(fichier,"%d",&nrows);
      fscanf(fichier,"%d",&ncols);
      matrice =(int*)calloc(nrows*ncols,sizeof(int));
      if (my_id == 0)		/* si je suis le processeur 0 alors je r�cup�re la matrice */
	{
	  int i,j;
	  for(i=0; i<nrows; i++)
	    {
	      for(j=0; j<ncols; j++)
		{
		  fscanf(fichier,"%d",&matrice[i*ncols + j]);
		}
	    }
	}
      fclose(fichier);
            
      taille_grille = MIN((int) floor(sqrt(nb_proc)), MIN(nrows, ncols)); /* la taille de la grille des processeurs 2D  */
      /* RAPPEL: la grille des processeurs est un carr� !! */
	
	
      // -- On met en place les commutateurs -- //	 
      // -------------------------------------- //      
      init_communicateurs();	/* cr�e les communicateurs Horizontal et Vertical */
      


      // POUR TOUS LES PROCESSEURS DONT ON AURA BESOIN: 
      if (my_id < taille_grille*taille_grille) /* si je ne suis pas un proc. "d�chet" */
	{       
	  // on initialise chaque processeur (appel l'initialisation it�rative/locale) //
	  // ------------------------------------------------------------------------- //
	  matrix_t A; /* cr�ent et calloc les registres etc.. de toutes les cellules du processeur courant */
	  int hauteur, largeur;	 /* dimension de la sous-matrice de A */
	  int ligne, colonne; /* coordonn�es dans le r�seau de processeurs de ce processeur */
	  
	  MPI_Comm_rank(MPI_HORIZONTAL, &colonne); /* le rang horizontal */
	  MPI_Comm_rank(MPI_VERTICAL, &ligne);	   /* le rang vertical */
	  
	  if (ligne < taille_grille - 1)
	    hauteur = nrows / taille_grille; /* dimension normale */
	  else 
	    hauteur = nrows - (taille_grille - 1) * (nrows / taille_grille); /* cases qu'il reste */
	  if ( colonne < taille_grille - 1)
	    largeur = ncols / taille_grille;
	  else
	    largeur = ncols - (taille_grille - 1) * (ncols / taille_grille);
	  
	  mat_init(&A, largeur, hauteur);
	  

	 // -- On distribue la matrice du probl�me dans le r�seau (par petits bouts)  -- //
	 // ---------------------------------------------------------------------------- //
	 if (my_id == 0)
	   {
	     int k,l = 0;
	     for(k=0;k<A.row;k++)
	       {
		 for(l=0;l<A.col;l++)
		   {
		     A.data[k*A.col+l] = matrice[k*ncols + l];
		   }
	       }
	     // maintenant on va trasmettre � chaque processeur sa matrice
	     int p,q;
	     for(p=0;p<taille_grille;p++)
	       {
		 for(q=0;q<taille_grille;++q)
		   {
		     if ((p != 0) || (q != 0))
		       {
			 // on va envoyer au processeur (p,q) sa matrice
			 int hauteur_locale, largeur_locale; /* dimensions de la matrice de (p,q) */
			 int ligne = p;
			 int colonne = q;
			 if (ligne < taille_grille - 1)
			   hauteur_locale = nrows / taille_grille; /* dimension normale */
			 else 
			   hauteur_locale = nrows - (taille_grille - 1) * (nrows / taille_grille); /* cases qu'il reste */
			 if ( colonne < taille_grille - 1)
			   largeur_locale = ncols / taille_grille;
			 else
			   largeur_locale = ncols - (taille_grille - 1) * (ncols / taille_grille);
			 int* mat = calloc(hauteur_locale*largeur_locale,sizeof(int));
			 int li,co; // les indices de k et l dans la grosse matrice
			 for(k=0;k<hauteur_locale;++k)
			   {
			     for(l=0;l<largeur_locale;++l)
			       {
				 // k et l sont les indices locaux
				 if (p < taille_grille-1)
				   li = (p*hauteur_locale) + k;
				 else
				   li = p*(nrows / taille_grille) + k;
				 if (q < taille_grille-1)
				   co = (q * largeur_locale) + l;
				 else
				   co = q*(ncols / taille_grille) + l;
				 // finalement, on fait l'affectation
				 mat[k*largeur_locale+l] = matrice[li * ncols + co ];
			       }
			   }
			 // on fait l'envoi demat vers le processeur (p,q)
			 MPI_Send(mat, hauteur_locale*largeur_locale, MPI_INT, taille_grille*p+q, 0, MPI_COMM_WORLD);
		       }
		   }
	       }
	   }
	 else			/* alors je re�ois data du processeur 0 */
	   {
	     MPI_Recv(A.data, hauteur*largeur, MPI_INT, 0, 0, MPI_COMM_WORLD, &statut_null);
	   }
	 MPI_Barrier(MPI_COMM_WORLD);
	 
	 // On initialise correctement le M et mini de chaque processeurs -- //
	 // ---------------------------------------------------------------- //
	 int a;
	 for (a = 0; a < A.row*A.col; a++)
	   {
	     A.M[a] = 0;
	     A.mini[a] = 0;
	   }
	 
	 
	 // On lance le calul dans la grille //
	 // -------------------------------- //
	 update(&A);
	 
	 // on va chercher le r�sultat: dans le M de la derni�re cellule du dernier processeur //
	 // ---------------------------------------------------------------------------------- //
	 int res[3];
	 if (my_id == taille_grille * taille_grille - 1) /* le dernier processeur qui est vraiment sur la grille (pas un d�chet) */
	   {
	     printf("L'AIRE DU RECTANGLE MAXIMAL EST: %d\n", A.M[A.row * A.col -1]);
	     res[0] = A.rang_x_max2[A.row * A.col -1];//la premi�re coordonn�e d�j� calcul�e
	     res[1] = A.rang_y_max2[A.row * A.col -1];//la deuxi�me coordonn�e d�j� calcul�e
	     res[2] = A.M[A.row * A.col -1]; // le r�sultat
	     if (my_id != 0) // il n'y a pas qu'un seul processeur
	       MPI_Send(res,3,MPI_INT,0,1,MPI_COMM_WORLD);
	   }
	 if (my_id == 0)
	   {
	     printf("yes");
	     if (nb_proc != 1)
	       MPI_Recv(res,3,MPI_INT,taille_grille * taille_grille - 1,1,MPI_COMM_WORLD,&statut_null);
	     // On va chercher les coordonn�es du haut :
	     int* somme = (int*) calloc(ncols*nrows,sizeof(int));
	     somme[res[0]*ncols+res[1]] = matrice[res[0]*ncols+res[1]];
	     int i,j;
	     int hg1 = 0; 
	     int hg2 = 0; // les coordonn�es que l'on va renvoyer � la fin du calcul
	     if(matrice[res[0]*ncols+res[1]] == res[2])
	       {
		 hg1 = res[0];
		 hg2 = res[1];
	       }
	     else
	       for(i=0;res[0]-i>=0;i++)
		 {
		   for(j=0;res[1]-j>=0;j++)
		     {
		       if(i != 0 || j!=0)
			 {
			   if (i==0)
			     {
			       //printf("i=0\n");
			       int li = (res[0]-i);
			       int co = (res[1]-j);
			       somme[li*ncols+co] = matrice[li*ncols+co] + somme[li*ncols+co+1];
			     }
			   else // i>0
			     {
			       if (j==0)
				 {
				   int li = (res[0]-i);
				   int co = (res[1]-j);
				   //printf("On monte, %d, %d,valeur du dessous : %d, on ajoute : %d, r�sultat : %d \n \n",res[0]-i,res[1]-i,somme[(li+1)*ncols + co],matrice[li*ncols+co],matrice[li*ncols+co] + somme[(li+1)*ncols + co]);
				   somme[li*ncols+co] = matrice[li*ncols+co] + somme[(li+1)*ncols + co];
				 }
			       else // i>0, j>0
				 {
				   int li = (res[0]-i);
				   int co = (res[1]-j);
				   somme[li*ncols+co] = matrice[li*ncols+co] + somme[(li+1)*ncols+co] + somme[li*ncols+(co+1)] - somme[(li+1)*ncols+(co+1)];
				 }
			     }
			 }
		       //printf("ligne : %d, colonne :  %d et somme[ligne][colonne] == %d\n",res[0] - i,res[1] - j,somme[(res[0]-i)*ncols+(res[1]-j)]);
		       
		       if(somme[(res[0]-i)*ncols+(res[1]-j)] == res[2])
			 {
			   hg1 = res[0] - i;
			   hg2 = res[1] - j;
			   break;
			 }
		       else
			 {}
		       //printf("ligne : %d, colonne :  %d et tout va bien\n",res[0] - i,res[1] - j);
		     }
		 }
	     if (somme[hg1*ncols+hg2] == res[2])
	       {
		 printf("Les coordonnees finales sont (%d,%d) et (%d,%d)\n",hg1,hg2,res[0],res[1]);
	       }
	     else
	       {
		 printf("Bug : Mauvaises coordonnees : (%d,%d) et (%d,%d)",hg1,hg2,res[0],res[1]);
	       }
	   }
	}
    }
  MPI_Finalize();
  
  return (0);
};
