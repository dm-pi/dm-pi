/*****************************************************************/
/*                                                               */
/* Produit de matrice par double diffusion                       */
/*                                                               */
/*****************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <mpi.h>
#include <math.h>
#include <stdarg.h>
#include <time.h>

/******* Fonctions d'affichage ***********/
#define VOIRD(expr) do {printf("P%d (%d,%d) <%.3d> : \t{ " #expr " = %d }\n", \
                               my_id,i_row,i_col,__LINE__,expr);fflush(NULL);} while(0)
#define PRINT_MESSAGE(...) FPRINT_MESSAGE(my_id,i_row,i_col, __LINE__,  __VA_ARGS__)

static void FPRINT_MESSAGE(int id, int row, int col, int line, const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  printf("P%d (%d,%d) <%.3d> : \t",id,row,col,line);
  vprintf(fmt, ap);
  fflush(NULL);
  va_end(ap);
  return;
}


typedef struct { 
  int row;   /* le nombre de lignes   */
  int col;   /* le nombre de colonnes */
  double* data; /* les valeurs           */
} matrix_t;

int my_id, nb_proc;
int nb_col,nb_row;       /* Largeur et hauteur de la grille */
MPI_Comm MPI_HORIZONTAL; /* Communicateur pour diffusions horizontales */
MPI_Comm MPI_VERTICAL;   /* Communicateur pour diffusions verticales */
int i_col,i_row;         /* Position dans la grille */


/*******************************************/
/* initialisation al�atoire de la matrice  */
/*******************************************/
void mat_init_alea(matrix_t *mat,int width, int height, int flag) /* si flag = 1: vrai al�atoire < 10 sinon du faux */
{
  int i,rh,rv;

  mat->row = height;
  mat->col = width;

  MPI_Comm_rank(MPI_VERTICAL,&rv);
  MPI_Comm_rank(MPI_HORIZONTAL,&rh);

  mat->data=(double*)calloc(height*width,sizeof(double));
  for(i=0 ; i<height*width ; i++)
    if (flag)
      mat->data[i]=(double)((int)(10.0*rand()/(RAND_MAX+1.0)));
    else
      mat->data[i]=rh+rv;
}

/*******************************************/
/* initialisation � 0 de la matrice        */
/*******************************************/
void mat_init_empty(matrix_t *mat,int width, int height)
{
  mat->row = height;
  mat->col = width;
  mat->data=(double*)calloc((mat->row)*(mat->col),sizeof(double));
}

/*******************************************/
/* affichage de la matrice                 */
/*******************************************/
void mat_display(matrix_t A)
{
  int i,j,t=0;

  printf("      ");
  for(j=0;j<A.col;j++)
    printf("%.3d ",j);
  printf("\n");
  printf("    __");
  for(j=0;j<A.col;j++)
    printf("____");
  printf("_\n");

  for(i=0;i<A.row;i++)
    {
      printf("%.3d | ",i);
      for(j=0;j<A.col;j++)
	printf("%.3g ",A.data[t++]);
      printf("|\n");
    }
  printf("    --");
  for(j=0;j<A.col;j++)
    printf("----");
  printf("-\n");
}

/*******************************************/
/* C+=A*B                                  */
/*******************************************/
void mat_mult(matrix_t A, matrix_t B, matrix_t C)
{
  int i,j,k,M,N,K;
  double *_A,*_B,*_C;
 
  _A=A.data;
  _B=B.data;
  _C=C.data;

  M = C.row;
  N = C.col;
  K = A.col;

  if((M!=A.row) || (N!=B.col) || (K!=B.row)) {
    PRINT_MESSAGE("Attention, tailles incompatibles");
    VOIRD(A.row);VOIRD(A.col);VOIRD(B.row);
    VOIRD(C.col);VOIRD(C.row);VOIRD(C.col);
    exit(1);
  }

  for(i=0 ; i<M ; i++)
    for(j=0 ; j<N ; j++)
      for(k=0 ; k<K ; k++) 
	_C[i*N+j]+=_A[i*K+k]*_B[k*N+j];
}

/*******************************************/
/* Initialisation de la grille             */
/*******************************************/
void init_communicateurs(void)
{
  /* Am�liorez la disposition */
  int m = (int) round(sqrt(nb_proc)); /* le plus simple: car on a une matrice carr�e */
  nb_col = m;
  nb_row = m;
  int abscisse = my_id / m;
  int ordonnee = my_id - (abscisse * m);

  /* Partitionnez MPI_COMM_WORLD en communicateurs pour les lignes 
     et en communicateurs pour les colonnes */
  MPI_Comm_split ( MPI_COMM_WORLD, ordonnee, abscisse, &MPI_HORIZONTAL); /* une ligne de proc (m�me ordonnee, num�ro = abscisse) */
  MPI_Comm_split ( MPI_COMM_WORLD, abscisse, ordonnee, &MPI_VERTICAL);   /* une colonne de proc (m�me abscisse, num�ro = ordonn�e) */
}

/*******************************************/
/* Produit de matrice par double diffusion */
/*******************************************/
void parallel_mat_mult(matrix_t A, matrix_t B, matrix_t C, matrix_t D, matrix_t E)
{
  /*
    Les matrices:
    -- A; B et C: on veut C = A B
       Mais dans A et B, on mettra les r�sultats du broadcast donc elles contiendront les A et B d'autres processeurs.
       C contient le calcul local de A * B 
    -- D et E contiennent toujours les matrices INITIALES A et B du processeur
   ((    -- temp1 et temp2 sont des double* pour assigner A = D tout en se souvenant de 1 (pareille pour B)))
   */
  int k, i, j, col;  
  int m = (int) round(sqrt(nb_proc)); 
  int abscisse;
  int ordonnee;
  //  double* temp1 = (double*)calloc(A.row*A.col,sizeof(double));
  //  double* temp2 = (double*)calloc(B.row*B.col,sizeof(double));

  MPI_Comm_rank(MPI_HORIZONTAL, &abscisse);
  MPI_Comm_rank(MPI_VERTICAL, &ordonnee);
  for (i=1; i<A.row*A.col; i++)	/* on initialise D et E pour se souvenir de A et B */
    {
      D.data[i] = A.data[i];
      E.data[i] = B.data[i];
    }
  
  for(k=0; k < m ; k++) {	/* c'est la boucle qui fait a_i,k * b_k,j sur les sous-matrices*/
    if (abscisse == k) /* si on est concern� par le calcul: en effet le rang dans HORIZONTAL = abscisse */
      {
	for (i = 1; i < A.row*A.col; i++)
	  {
	    A.data[i] = D.data[i];
	  }
      }
      /* diffusion horizontale */
      MPI_Bcast (A.data, A.col*A.row, MPI_DOUBLE, k, MPI_HORIZONTAL);
    
    if (ordonnee == k)  /* si on est concern� par le calcul: en effet le rang dans VERTICAL = ordonnee */
      {
	for (i = 1; i < A.row*A.col; i++)
	  {
	    B.data[i] = E.data[i];
	  }
      }
      /* diffusion verticale */
      MPI_Bcast (B.data, B.col*A.row, MPI_DOUBLE, k, MPI_VERTICAL);

    /* produit de matrices en local */
    mat_mult(A, B, C);
  }
}


int main(int argc, char **argv) 
{
  int taille=0;
  int flag = 1;			/* du vrai al�atoire */

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&nb_proc);
  MPI_Comm_rank(MPI_COMM_WORLD,&my_id);
  
  if(argc==0)
    fprintf(stderr,"Usage : %s <taille de matrice>",argv[0]);
  else 
    taille = atoi(argv[1]);	/* taille = nombre de processeurs */
  
  init_communicateurs();	/* cr�e les communicateurs Horizontal et Vertical */
  
  {
    matrix_t A,B,C,D,E;

    srand((time_t)(my_id*time(NULL)));
    
    mat_init_alea(&A,taille/nb_col,taille/nb_row, flag);
    mat_init_alea(&B,taille/nb_col,taille/nb_row, flag);
    mat_init_empty(&C,taille/nb_col,taille/nb_row);
    mat_init_empty(&D,taille/nb_col,taille/nb_row);
    mat_init_empty(&E,taille/nb_col,taille/nb_row);
 
    int temps = 0;
    int dec = 1;
    if( my_id == 0)
      printf(" --> Taille: %d et nombre de processeurs: %d.\n", taille, nb_proc);
    if( my_id == 0)
      printf(" --> Nombre de colonnes %d et nombre de lignes %d (dans les matrices de chaque processeur)).\n", nb_col, nb_row);
    if( my_id == 0)
      {   
	printf(" A init en haut � gauche : \n");
	printf("my_id = %d .\n", my_id);
	mat_display(A);
      }
    temps += dec;
    
    if( my_id == 1)
      {
	sleep(temps);
	printf(" bout en haut � droite de A init (my_id = 1 ok si taille = 4 = nb_procs): \n");
	printf("my_id = %d .\n", my_id);
	mat_display(A);
      }
    temps += dec;
    
    
    if( my_id == 0) {
      sleep(temps);
      printf(" B init en haut � gauche : \n");
      printf("my_id = %d .\n", my_id);
      mat_display(B);
    }
    temps += dec;

    if(my_id==2)
      {
	sleep(temps);
	printf(" bout en bas � gauche de B init (my_id = 2 ok si taille = 4 = nb_procs): \n");
	printf("my_id = %d .\n", my_id);
	mat_display(B);
      }
  
    parallel_mat_mult(A,B,C,D,E);

    if( my_id == 0)
      {
	printf(" C: \n");
	mat_display(C);
      }
  }
  MPI_Finalize();
  
  return (0);
}
