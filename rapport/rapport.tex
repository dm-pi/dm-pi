\documentclass[a4paper, 11pt]{article}
\usepackage[french]{babel}                                 % normes typographiques
\usepackage[utf8]{inputenc}                                % les accents
\usepackage[T1]{fontenc}                                   % caractères francais
\usepackage{amsmath,amssymb,mathrsfs,amsthm}               % packages de l'AMS pour les maths
\usepackage{hyperref}                                      % pour gérer les liens dans les pdf
\usepackage[margin=2.7cm]{geometry}                                     % les marges
\usepackage{graphicx}                                      % inclure des images
\usepackage[all]{xy}                                       % x-fig
\usepackage{color}                                         % colorer
%\usepackage{tikz}                                          % pour dessiner (graphes par ex)
\usepackage{multirow}                                      % pour fusionner des lignes dans des tabs

\usepackage[french,linesnumbered, algoruled]{algorithm2e}  % mettre
% des algos

\hypersetup{
  colorlinks,%
  citecolor=black,%
  filecolor=black,%
  linkcolor=black,%
  urlcolor=black
}

\definecolor{grey}{rgb}{0.95,0.95,0.95} 
\definecolor{rouge_sombre}{rgb}{0.8,0,0}
\definecolor{rouge}{rgb}{0.6,0,0}
\definecolor{mauve}{rgb}{0.4,0,0.2}

\newtheorem{defi}{Définition}
\newtheorem{prop}{Proposition}
\newtheorem{thm}{Théorème}
\newtheorem{lem}{Lemme}
\newtheorem{cor}{Corollaire}
\newtheorem{prob}{Problème}

\newcommand{\voc}[1]{{\textcolor[rgb]{0.4,0,0}{\textbf{#1}}}}
\newcommand{\ex}[1]{{\textcolor{green}{\textbf{#1}}}}
\newcommand{\grandO}[1]{O\mathopen{}\left(#1\right)}
\newcommand{\newl}{\hfill \\}

\title{Devoir à la maison d'algorithmique parallèle\\Maximum subarray problem}
\author{Lucca Hirschi \& Thomas Sibut-Pinote}
\date{\today}

\SetKwIF{Si}{SinonSi}{Sinon}{Si}{alors}{sinon si}{sinon}{finsi}
\SetKwRepeat{Repeter}{Répéter}{jusqu'à ce que}
\SetKwFor{Tq}{Tant que}{faire}{fin tant que}
\SetKw{ret}{Renvoyer}
\SetKw{Init}{Initialisation: }
\SetKwFor{Pour}{Pour}{faire}{FinPour}
%\SetAlgoVlined
\SetSideCommentRight
%\LinesNumbered
\SetCommentSty{texttt}
\SetCommentSty{footnotesize}
%\SetNlSty{texttt}{}{}
%\SetNlSkip{10pt}


\begin{document}
\maketitle
\tableofcontents

\section{Le problème}
\label{sec:le-probleme}
Le problème que l'on considère est le problème de la sous-matrice
maximale. Soit $A\in\mathbb{N}^{n,m}$. Nous noterons
$A[i_1,i_2][j_1,j_2]$ la sous-matrice de $A$ commençant en $(i_1,i_2)$
et finissant en $(j_1,j_2)$. Nous désignons $\sum_{i=0}^{n-1}\sum_{j=0}^{m-1}a_{i,j}$ par $\mathcal{A}(A)$. Le problème s'énonce alors comme cela:
\begin{prob}[Sous-matrice maximale]
  Soit $A\in\mathbb{N}^{n,m}$ une matrice. On cherche \linebreak[4]
  $0\le i_1 <
  i_2\le n-1$ et $0\le j_1 < j_2\le m-1$ tels que $\forall k_1, k_2,
  l_1, l_2,\ 0\le k_1 < 
  k_2\le n-1$ et $0\le l_1 < l_2\le m-1$, 
  \[\mathcal{A}(A[i_1,i_2][j_1,j_2])\ge \mathcal{A}(A[k_1,k_2][l_1,l_2]).\]
\end{prob}
En d'autres termes, on cherche la sous-matrice d'aire maximale.
Nous considérons ce problème avec une restriction sur les valeurs des
matrices: ce sont des entiers compris entre $3200$ et $-3200$.
La matrice est initalement stockée dans un fichier de la forme:
\begin{center}
  \begin{minipage}[H]{100pt}
    $n\ m$\\
    $4$ $5$ $6$\\
    \dots
  \end{minipage}
\end{center}
Où $n$ est le nombre de lignes et $m$ le nombre de colonne suivis de
$n$ lignes contenants chacune $m$ valeurs. Il est facile d'extraire
les données de ce fichier pour constituer la matrice en mémoire en
temps $O(mn)$.


\section{L'algorithme}
\label{sec:lalgorithme}

\subsection{Algorithmes itératifs}
\label{sec:algorithme-iteratif}

\paragraph{Algorithme naïf}
\label{sec:algorithme-naif}
L'algorithme naïf résolvant ce problème est en $O((n m)^3)$
opérations (on calcule l'aire de tous les rectangles possibles et on
prend le maximum).


\paragraph{Algorithme en $O(m^2 n)$}
\label{sec:algor-moins-naif}

On décrit informellement cet algorithme. Il est extrait de la
thèse~\cite{these}. L'algorithme que l'on
retiendra pour l'implémentation en est une version parallèle.

Nous commençons par pré-calculer les
\[sum[i][j] = \sum_{0\le p \le i\ 0\le q\le j}{a_{q,p}}\]
en temps $O(mn)$. Car pour $i\ge 1$ et $j\ge 1$, on a 
\[sum[i][j] = sum[i-1][j]+sum[j-1][i]-sum[i-1][j-1] + a_{i,j}.\]
Désormais, nous calculons les
\[sum_{g,h}[j] = sum[h][j] - sum[g-1][j]\]
en temps $O(m^2n)$. On remarque que $sum_{g,h}[j]$ est l'aire du
rectangle commençant à la repmière ligne, g$^{ie}$-colonne et termine à
la $j^{ie}$-ligne, $h^{ie}$-colonne. On remarque alors qu'il suffit de
chercher $0\le g <h\le m-1$ et $0\le j_1<j_2\le n-1$ tels  que
$sum_{g,h}[j_2] - sum_{g,h}[j_1]$ soit maximale. La réponse au
problème est alors $(j_1,g) \ (j_2,h)$.

Pour calculer ce maximum, nous allons pour tout $g, h$ tels que $0\le g
<h\le m-1$ appliquer l'algorithme~\ref{fig:algo-1D} qui donne la
solution en temps $O(n)$. La complexité globale est alors en $O(m^2 n)$.
Bien sûr, on peut transformer l'algorithme pour qu'il soit en $O(mn^2)$.
\begin{figure}[H]
  \centering
    \begin{algorithm}[H]
    \SetKwFunction{Prod}{produit}
    \SetKwFunction{Initialisation}{Initialisation}
    \SetKw{Faire}{faire} \SetKwBlock{Bloc}{}{} \SetKw{main}{Maximum 1-D}
    \SetKw{Prod}{Produit }
\main
\Bloc{
  $min\leftarrow 0$\tcp*[r]{le minimum courant}
  $M\leftarrow 0$\tcp*[r]{le maximum courant (0 par défaut)}
  \Pour{$i=0$ à $n-1$}
  {
    $cand\leftarrow s_{g,h}[i] - min$\tcp*[r]{peut-être le nouveau maximum courant}
    $M\leftarrow MAX(M,cand)$\;
    $min\leftarrow MIN(min,s_{g,h}[i])$\;
}
\ret{$M$}
}
\end{algorithm}
  \caption{Algorithme de maximum en dimension 1}
\label{fig:algo-1D}
\end{figure}



\subsection{Algorithme parallèle}
\label{sec:algorithme-parallele}
On présente l'algorithme parallèle choisi pour résoudre le
problème. L'idée consiste à appliquer l'algorithme itératif décrit
plus tôt sur un réseau $2-D$ de processeurs contenants initialement un
bout de la matrice.

Supposons dans un premier temps que l'on dispose de $m n$
processeurs. On alloue un processeur au traitement d'une case de la
matrice. Chaque processeur ne connaît que ses 4 voisins (N, S, E et
O) sauf les processeurs sur la première ligne qui peuvent accéder à la
matrice du problème. Dans un premier temps, nous propageons les
valeurs de la matrice du problème vers le bas, afin que chaque
processeur $P_{i,j}$ dispose localement de la case $a_{i,j}$ de la
matrice du problème. Ce premier calcul coûte $O(n)$ étapes (un simple
all-to-all au sein de chaque colonne).


Ensuite chaque processeur lance en parallèle l'algorithme présenté
dans la figure~\ref{fig:algo-2D} (au début une fois
\texttt{Initialisation} puis $m+n$ fois
\texttt{update}). Il faut noter que pour $P_{i,j}$, toutes les données
de $P_{i-1,j}$ et $P_{i,j-1}$ sont accessibles au tour suivant. C'est
celà que l'on dénote en écrivant $s_{in\ i-1,j}$ par exemple.
\begin{figure}[H]
  \centering
    \begin{algorithm}[H]
    \SetKwFunction{Prod}{produit}
    \SetKwIF{Si}{SinonSi}{Sinon}{Si}{alors}{sinon si}{sinon}{finsi}
    \SetKw{fin}{Fin}
    \SetKw{Faire}{faire} \SetKwBlock{Bloc}{}{}
    \SetKw{mai}{Initialisation$_{i,j}$: }
\SetKw{maii}{Update$_{i,j}$: }
    \SetKw{Prod}{Produit }
\mai
\Bloc{
  $r_{i,j}\leftarrow 0$\tcp*[r]{la somme $a[i][0] + \dots a[i][j]$}
  $s_{i,j}\leftarrow 0$\tcp*[r]{la somme préfixe ($=sum[i][j]$ à la fin)}
  $min_{i,j}\leftarrow 0$\tcp*[r]{le minimum de la somme préfixe}
  $M_{i,j}\leftarrow 0$\tcp*[r]{le maximum de la somme préfixe et des
    voisins}
  \Si{$i = 0$}
  {
    S'active\tcp*[r]{Seul le tout premier processeur commence à
      travailler à l'étape 0}
  }
  \fin\;
}
\maii
\Bloc{
  \Si{$P_{i,j}$ est active alors}
  {
    $r_{i,j}\leftarrow r_{in\ i,j-1} + a_{i,j}$\;
    $min_{i,j}\leftarrow MIN(s_{in\ i,j-1},min_{in\ i,j-1})$\;
    $s_{i,j}\leftarrow s_{in\ i-1,j} + r_{i,j}$\;
    $cand_{i,j}\leftarrow s_{i,j} - min_{i,j}$\;
    $M_{i,j}\leftarrow MAX(M_{in\ i-1,j}, M_{in\ i,j-1}, M_{i,j},
    cand_{i,j})$\;
    activer $P_{i,j+1}$\tcp*[r]{On propage le contrôle
      avec les données}
  }
\fin\;
}
\end{algorithm}
  \caption{Algorithme de maximum en dimension 2}
\label{fig:algo-2D}
\end{figure}

\paragraph{Explications}
\label{sec:expliquations}
\texttt{Update} calcule la somme préfixe ($sum[i][j]$, le minimum
préfixe et le maximum préfixe). On note $a_{i,j}^k$ la valeur du
registre $a$ du processeur $P_{i,j}$ à l'étape $k$.


Sans corriger l'algorithme, je donne quelques propriétés de l'algorithme.
\begin{enumerate}
\item Pour $k\ge j$, $r_{i,j}^k = \sum_{q = 0}^{j}a_{i,q}$;
\item $s_{i,j}^k = sum_{g,i}[j]$ où $g = MAX(0,i+j-k)$;
\item $min_{i,j}^k = MIN_{0\le q\le j-1}(sum_{g,i}[q])$ où $g = MAX(0,
  i+j-k)$;
\item $M_{i,j}^k$ est la sous-matrice maximale de $A[g,i][0,j]$ où $g
  = MAX(0, i+j-k)$.
\end{enumerate}

Le résultat se trouve dans $M_{m,n}$ à l'étape $m+n$ (c'est-à-dire
$M_{m,n}^{m+n}$).

L'activation des processeurs se fait de la manière suivante: au départ, tous les processeurs de la première colonne sont actifs, puis ils propagent l'activation à leur voisin de droite.


\paragraph{Interêts}
\label{sec:interets}

Cet algorithme a l'avantage de parfaitement paralléliser les tâches, au sens où l'on peut utiliser autant de processeurs que l'on souhaite pour faire les calculs dans la limite du nombre de cases, c'est-à-dire $m*n$.
Bien sûr, en pratique on a beaucoup moins que $m*n$ processeurs disponibles, mais si on en a $p$, on utilise $(\lfloor \sqrt(p) \rfloor)^2$ processeurs, c'est-à-dire asymptotiquement $p$ : il y a peu de déchets.

De plus, cet algorithme utilise seulement des informations locales dans une grille en deux dimensions, c'est-à-dire que chaque processeur a au plus quatre voisins. Cela permet de minimiser les communications et donc de ne pas pénaliser le temps de calcul, d'autant plus que les informations transmises sont de taille $\mathcal{O}(max(m,n))$ (les buffers) : on ne transmet jamais une matrice complète. 

\textbf{Remarque} : Il ne faut pas compter la distribution de la matrice aux processeurs par le premier processeur dans le coût de l'algorithme, puisqu'en pratique cet algorithme est utilisé à la suite d'autres calculs parallèles et qu'on peut donc supposer que chaque processeur possède déjà sa partie de la matrice. Ceci ne constitue donc pas une exception à la dimension ``locale'' des communications.

\section{Implémentation de l'algorithme}
\label{sec:impl-de-lalg}
Premier problème: nous ne disposons pas de $mn$ processeurs. Nous
allons donc découper la grille en $p$ (le nombre de processeurs)
sous-grilles. Le calcul dans chaque sous-grille sera fait
itérativement (fonction \texttt{init$\_$ite} et
\texttt{update$\_$ite}). Entre les grilles, les calculs se font en
parallèle avec des communications sur des communicateurs
\texttt{HORIZONTAL} et \texttt{VERTICAL}.


\section{Résultats expérimentaux}
\label{sec:result-exper}

Les résultats expérimentaux sont consignés dans le fichier \texttt{graphes.pdf}.
On constate que notre algorithme n'apporte pas un speed-up extraordinaire pour les petites matrices (il est même inférieur à 1 pour une matrice $100 \times 200$). Par contre, il peut aller jusqu'à 15 pour 35 processeurs sur une matrice avec 6 millions de coefficients, ce qui donne une efficacité théorique de $0.44$.
Nous pensons que l'efficacité serait bien meilleure avec plus de processeurs sur de telles matrices, malheureusement nous avons été limités par le nombre d'ordinateurs des salles de travail de l'ENS \footnote{À ce propos, il serait peut-être bon de faire remarquer à tous les élèves de l'ENS, qui n'en sont pas nécessairement conscients, qu'une machine peut être utilisée même si personne n'est assis devant, et que par conséquent il ne faut pas éteindre les ordinateurs...}. Dans le cas de petites matrices, cet algorithme utilise des structures particulièrement lourdes, et beaucoup de communications. Cependant, ceci est un avantage sur des matrices beaucoup plus grosses, car les communications sont de taille relative peu élevée et uniquement avec de proches voisins.



\appendix

\bibliographystyle{plain}	% (uses file "plain.bst")
\bibliography{bib}{}

\end{document}
