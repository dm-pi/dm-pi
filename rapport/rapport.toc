\select@language {french}
\contentsline {section}{\numberline {1}Le probl\IeC {\`e}me}{1}{section.1}
\contentsline {section}{\numberline {2}L'algorithme}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Algorithmes it\IeC {\'e}ratifs}{1}{subsection.2.1}
\contentsline {paragraph}{Algorithme na\IeC {\"\i }f}{1}{section*.2}
\contentsline {paragraph}{Algorithme en $O(m^2 n)$}{2}{section*.3}
\contentsline {subsection}{\numberline {2.2}Algorithme parall\IeC {\`e}le}{2}{subsection.2.2}
\contentsline {paragraph}{Explications}{2}{section*.4}
\contentsline {paragraph}{Inter\IeC {\^e}ts}{3}{section*.5}
\contentsline {section}{\numberline {3}Impl\IeC {\'e}mentation de l'algorithme}{3}{section.3}
\contentsline {section}{\numberline {4}R\IeC {\'e}sultats exp\IeC {\'e}rimentaux}{3}{section.4}
