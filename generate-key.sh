#!/bin/sh
ssh-keygen -q -t dsa -f ~/.ssh/id_dsa_tp_mpi2 -N ""
cat ~/.ssh/id_dsa_tp_mpi2.pub >> ~/.ssh/authorized_keys
ssh-add ~/.ssh/id_dsa_tp_mpi2
for ((i = 0; i <= 9 ; i += 1))
do
A="slsu2-0"$i
ssh $A exit
done
for ((i = 0; i <= 9 ; i += 1))
do
A="slsu2-1"$i
ssh $A exit
done
for ((i = 0; i <= 9 ; i += 1))
do
A="slsu1-0"$i
ssh $A exit
done
for ((i = 0; i <= 9 ; i += 1))
do
A="slsu1-1"$i
ssh $A exit
done
for ((i = 0; i <= 9 ; i += 1))
do
A="slsu0-0"$i
ssh $A exit
done
for ((i = 0; i <= 9 ; i += 1))
do
A="slsu0-1"$i
ssh $A exit
done
for ((i = 0; i <= 9 ; i += 1))
do
A="slsu0-2"$i
ssh $A exit
done